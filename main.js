let laptopData
let bank
let work
let laptopArea

init()

async function init() {
    laptopData = await getLaptopData()
    bank = new Bank()
    work = new Work()
    laptopArea = new LaptopArea(laptopData)
    updateBalance()
    updatePay()
}

async function getLaptopData() {
    const url = "https://noroff-komputer-store-api.herokuapp.com/computers"
    const response = await fetch(url)
    return await response.json()
}

function loanBtn() {
    bank.newLoan()
}

function workBtn() {
    work.work()
}

function bankBtn() {
    work.getSalary()
}

function repayLoanBtn() {
    work.repayLoan()
}

function buyBtn() {
    laptopArea.buyLaptop()
}


/* === FUNCTIONS USED TO UPDATE THE DOM === */

// Hide the loan text when there is no active loan
function toggleLoan() {
    document.querySelector(".active-loan").classList.toggle("none")
    document.querySelector(".btn-repay-loan").classList.toggle("none")
}

function updateLoan() {
    document.querySelector("#current-loan").textContent = getCurrency(bank.activeLoanAm)
}

function updateBalance() {
    document.querySelector("#current-balance").textContent = getCurrency(bank.balance)
}

function updatePay() {
    document.querySelector("#pay").textContent = getCurrency(work.currentPay)
}

function updateLaptopFeatures(id) {
    laptopArea.updateLaptopInformation(id)
}
/* ======================================== */


// Format a number to currency string
function getCurrency(value) {
    return new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(value)
}