class LaptopArea {
    constructor(laptopData) {
        this.laptops = laptopData
        this.currentLaptop

        this.populateDropdown()
        this.updateLaptopInformation(1)
    }

    populateDropdown() {
        const parent = document.querySelector("#laptops")
        parent.innerHTML = ""

        this.laptops.forEach(laptop => {
            parent.innerHTML += getElement(laptop.id, laptop.title)
        })

        function getElement(id, title) {
            return `<option value="${id}">${title}</option>`
        }
    }

    // Display information about the currently selected laptop
    updateLaptopInformation(id) {
        this.currentLaptop = this.getLaptopById(parseFloat(id))
        console.log(this.currentLaptop)
        document.querySelector("#laptop-name").textContent = this.currentLaptop.title
        document.querySelector(".laptop-img").src = `https://noroff-komputer-store-api.herokuapp.com/${this.currentLaptop.image}`
        document.querySelector("#laptop-desc").textContent = this.currentLaptop.description
        document.querySelector("#laptop-price").textContent = getCurrency(this.currentLaptop.price)
        this.updateLaptopFeatures(this.currentLaptop)
    }

    // Display the features of the currently selected laptop
    updateLaptopFeatures(laptop) {
        const parent = document.querySelector("#laptop-features")
        parent.innerHTML = ""
        laptop.specs.forEach(feature => {
            const element = `<div class="laptop-feature-text">${feature}</div>`
            parent.innerHTML += element
        })
    }

    getLaptopById(laptopId) {
        return this.laptops.find(({ id }) => id === laptopId);
    }

    buyLaptop() {
        // Try to buy the selected laptop. If payment is successfull the bank will return true.
        let success = bank.transfer(this.currentLaptop.price)
        if (success == false) return
        alert(`RECEIPT \n----------\n${this.currentLaptop.title}: ${getCurrency(this.currentLaptop.price)} \n----------\nTotal: ${getCurrency(this.currentLaptop.price)}`)
    }
}