class Bank {
    constructor() {
        this.balance = 200
        this.activeLoanAm = 0
        this.hasLoan = false

    }

    payOfLoan(am) {
        if (this.hasLoan == false) return false

        // If user is paying of loan with more money than the remaining loan
        if (this.activeLoanAm - am <= 0) {
            this.balance += Math.abs(this.activeLoanAm - am)
            this.activeLoanAm = 0
            this.hasLoan = false
            toggleLoan()
        }
        else {
            this.activeLoanAm -= am
        }

        updateBalance()
        updateLoan()
        return true
    }

    newLoan() {
        if (this.hasLoan) {
            alert("You already have a loan.")
            return
        }

        let input = prompt("How much do you want to loan: ")
        let loanAm = parseFloat(input)

        if (isNaN(loanAm)) {
            alert(`"${input}" is not a valid amount.`)
            return
        }

        if (loanAm <= 0) {
            alert(`You must loan more than ${getCurrency(0)}`)
            return
        }

        if (loanAm / 2 > this.balance) {
            alert("You cannot loan for more than 200% of your balance.")
            return
        }

        this.hasLoan = true
        this.activeLoanAm = loanAm
        this.balance += loanAm

        updateBalance()
        toggleLoan()
        updateLoan()
    }

    // Recieve money from working
    receive(am) {
        if (this.hasLoan) {
            let loanPay = this.getPercentage(am, 10)
            let remainder = am - loanPay

            this.payOfLoan(loanPay)
            this.balance += remainder
        }
        else {
            this.balance += am
        }
        updateBalance()
        return true
    }

    // returned value represents if the transfer was successful or failed
    transfer(am) {
        if (this.balance < am) {
            alert(`You cannot afford to transfer ${getCurrency(am)} with a balance of ${getCurrency(this.balance)}.`)
            return false
        }
        this.balance -= am
        updateBalance()
        return true
    }

    getPercentage(am, percent) {
        return (am / 100) * percent;
    }
}