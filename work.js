class Work {
    constructor() {
        this.currentPay = 0
        this.payAm = 100
    }

    work() {
        this.currentPay += this.payAm
        updatePay()
    }

    // Receive your current salary
    getSalary() {
        let success = bank.receive(this.currentPay)
        if (success == false) return
        this.currentPay = 0
        updatePay()
    }

    repayLoan() {
        let success = bank.payOfLoan(this.currentPay)
        if (success == false) return
        this.currentPay = 0
        updatePay()
    }
}